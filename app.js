const express = require('express');
const hbs = require('hbs');
require('dotenv').config()

const app = express();
const port = process.env.PORT;

// handdlerBar
app.set('view engine', 'hbs');
hbs.registerPartials(__dirname + '/views/partials');

// servir contenido estatico
app.use(express.static('public'));

app.get('/', (req, res) => {
  res.render('home', {
    nombre: 'maxi poblete',
    titulo: 'test web server'
  });
});

app.get('/generic', (req, res) => {
  res.render('generic', {
    nombre: 'maxi poblete',
    titulo: 'test web server'
  })
});

app.get('/elements', (req, res) => {
  res.render('elements', {
    nombre: 'maxi poblete',
    titulo: 'test web server'
  })
});

app.get('*', (req, res) => {
  res.sendFile(__dirname + '/back/public/404.html')
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
})
